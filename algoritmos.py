# -*- coding: utf-8 -*-
#Conversão de-para para rnp
def dfs(graph, start):
    """
        Realiza uma busca em profundidade num grafo

        Parâmetros:
            graph   -- dicionário com o grafo
            start   -- nó de início da busca
        Saída:
            visited -- nós visitados pela busca
    """
    visited, stack = [], [[start, 1]]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.append(vertex)
            if vertex[0] in graph.keys():
                tmp = set([i[0] for i in visited]).intersection(graph[vertex[0]]).symmetric_difference(graph[vertex[0]]),
                for i in tmp[0]:
                    stack.append([i, vertex[1]+1])
    visited = map(list, zip(*visited))              #Transposição da rnp
    return visited

def dp2rnp(dePara, roots):
    """
        Gera a floresta com todas as rnp's da rede

        Parâmetros:
            case     -- dicionário com os dados da rede elétrica
        Saída:
            floresta -- rnps num dicionário
       
    """
    paraDe = [(j, i) for i, j in dePara]            #Inversão necessária
    #Transforma o De-Para num grafo
    dp = dict()
    for i in dePara:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    for i in paraDe:     
        if dp.has_key(i[0]):
            dp[i[0]].append(i[1])
        else:
            dp[i[0]] = [i[1]]
    floresta = dict()
    for n, i in enumerate(roots): #For com todos os nós raizes para gerar todas as rnps
        tmp = dfs(dp, i)
        floresta[n] = tmp
    return floresta

def rnp2dp(rnp):
    """
        Transforma uma rnp num de-para

        Parâmetros:
            rnp     -- representação nó profundidade
        Saída:
            dePara  -- de-para
    """
    dePara = []
    #Inverte a RNP
    rnp = [rnp[0][::-1], rnp[1][::-1]]          #Inverte a rnp
    for n, (i, j) in enumerate(zip(*rnp)):
        for m, (k, l) in enumerate(zip(*rnp)[n+1:]):
            if l == j-1:
                dePara.append((k, i))
                break
    return dePara